# Generated by Django 4.0.6 on 2022-07-30 09:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_alter_trackingpoints_distance_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='trackingpoints',
            name='group',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
    ]
