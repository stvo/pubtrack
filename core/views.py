from django.shortcuts import render
import csv
from django.http import HttpResponse
from .models import trackingpoints as tp
import pytz
from datetime import datetime
import time
from geopy import distance


def tracker(request):
    data = request.GET
    print(data)
    tracker_name = data.get("name")
    lon = data.get("lon")
    lat = data.get("lat")
    group = data.get("group")
    # python does not like millisecods in timestamp -> 10 digits (seconds) is enough
    timestamp = int(data.get("timestamp")[0:10])
    tz = pytz.timezone("Europe/Berlin")
    time = datetime.fromtimestamp(timestamp, tz)
    try:
        last_track = tp.objects.filter(tracker_name=tracker_name).latest('timestamp')
        distance_div = distance.distance((lon, lat), (last_track.lon, last_track.lat)).meters
    except:
        distance_div = 0.0
    if not distance_div == 0:
        time_div = datetime.timestamp(time) - datetime.timestamp(last_track.timestamp)
        speed = round(distance_div / time_div * 3.6, 2)
        distance_div = round(distance_div, 2)
    else:
        speed = 0.0
    try:   
        tpEntry = tp.objects.create(tracker_name=tracker_name, lon=lon, lat=lat, timestamp=time, distance=distance_div, speed=speed, group=group)
        return HttpResponse(status=200)
    except:
        return HttpResponse(status=412)


def csv_response(request, group=''):
    tracker_ids = tp.objects.values_list('tracker_name', flat=True).distinct()
    response = HttpResponse(
            content_type='text/csv',
            headers={'Content-Disposition': 'attachment; filename="somefilename.csv"'},
        )
    writer = csv.writer(response)
    writer.writerow(['lat', 'lng', 'tracker_name', 'time', 'distance', 'speed'])
    # if there are any matching tracker ids continue
    if tracker_ids:
        check_content = False
        for tracker_id in tracker_ids:
            track_object = tp.objects.filter(tracker_name=tracker_id).latest('timestamp')
            #show only trackers who provided data within the last hour (3600 seconds)
            if time.time() - datetime.timestamp(track_object.timestamp) < 3601:
                if not group == '':
                    # only return object of group fitting url
                    if track_object.group == group:
                        ## only set to true if there were trackers in the last 5 minutes
                        check_content = True
                        writer.writerow([track_object.lat, track_object.lon, track_object.tracker_name, track_object.timestamp, track_object.distance, track_object.speed])
                    # only return objects without a group (empty string, null or the actual group "nogroup" are considered nogroup)
                    elif group == 'nogroup' and ( track_object.group == '' or track_object.group == 'nogroup' or not track_object.group ):
                        ## only set to true if there were trackers in the last 5 minutes
                        check_content = True
                        writer.writerow([track_object.lat, track_object.lon, track_object.tracker_name, track_object.timestamp, track_object.distance, track_object.speed])
                # return all trackers, with and without group
                else:
                    ## only set to true if there were trackers in the last 5 minutes
                    check_content = True
                    writer.writerow([track_object.lat, track_object.lon, track_object.tracker_name, track_object.timestamp, track_object.distance, track_object.speed])
                # only send content response if any tracker data was added
        if check_content:
            return response
    # we'll end up here if either there are no tracker_ids (empty db) or they are all out of date
    return HttpResponse(status=412)
    