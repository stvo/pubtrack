"""
URLs for core backend

"""
from django.urls import path
from . import views


urlpatterns = [
    path('tracker', views.tracker, name='tracker'),
    path('csv', views.csv_response, name='csv'),
    path('csv/<str:group>', views.csv_response, name='csv_group'),
]