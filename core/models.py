from django.db import models


class trackingpoints(models.Model):
    tracker_name = models.CharField(max_length=100)
    lon = models.CharField(max_length=20)
    lat = models.CharField(max_length=20)
    timestamp = models.DateTimeField()
    created = models.DateTimeField(auto_now_add=True)
    distance = models.FloatField()
    speed = models.FloatField()
    group = models.CharField(max_length=30,null=True,blank=True)



    def __str__(self):
        return str(self.tracker_name)

    objects = models.Manager()