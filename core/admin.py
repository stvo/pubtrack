from django.contrib import admin
from .models import trackingpoints as tp



class tpAdmin(admin.ModelAdmin):
    list_display = ('tracker_name', 'lon', 'lat', 'timestamp', 'created')

admin.site.register(tp, tpAdmin)