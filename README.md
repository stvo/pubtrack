# Pubtrack - Track your moves and publish it

This code is used as a logger backend and publishes the received data as an csv to be used in a umap (or wherever), to show current positions of groups etc.

## How to run it

for Debian/Ubuntu:

```bash
# we need virtualenv and pip
apt install python3-venv python3-pip
```

choose and create dir for django, for example /opt/tracker

```bash
cd /opt/tracker
#create virtualenv and activate it
python3 -m venv venv
source venv/bin/activate
#clone repo
git clone git@codeberg.org:stvo/pubtrack.git
# install dependencies
pip install -r requirements.txt
```

now you can just run the app with

```bash
cd pubtrack
#you should never use the django runserver command in a production environment! Testing only!
./manage.py runserver 0.0.0.0:80
```

## Logger settings

Phonetrack:
<http://example.com/tracker?lat=%LAT&lon=%LON&timestamp=%TIMESTAMP&name=SOMENAME>

OSMAnd:
<http://example.com/tracker?lat={0}&lon={1}&timestamp={2}&name=SOMENAME>

GpsLogger:
<http://example.com/tracker?lat=%LAT&lon=%LON&timestamp=%TIMESTAMP&name=SOMENAME>

The data for lat, lon, timestamp and name are required
Optional a group can be added by adding "&group=examplegroup" -> instead of "examplegroup" use your own groupname(s)

## How to use it

<http://example.com/csv> will provide a csv file with the latest positions of all your trackers and the timestamp they were provided.
You can use this csv for example as an external datasource in umaps

If using groups the trackers belonging to a specific group e.g. "examplegroup" can be received with <http://example.com/csv/examplegroup>. With <http://example.com/csv/nogroup> all trackers without a group or with the group "nogroup" can be received.
